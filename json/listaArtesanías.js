var artesanías = [{
    id: 1,
    nombre: "Maceta Piedra",
    descripcion: "Macetas hermosas para todas tus plantas",
    foto: "imgs/artesanías/artesanía_01.jpg",
    tipoDePublicación: 'Venta',
    precio: "$1500"
  },
  {
    id: 2,
    nombre: "Imanes",
    descripcion: "Imanes ideales para la heladera",
    foto: "imgs/artesanías/artesanía_02.jpg",
    tipoDePublicación: 'Venta',
    precio: "$200"
  },
  {
    id: 3,
    nombre: "Macetas con Dibujos",
    descripcion: "Macetas coloridas para adornar el frente de tu casa",
    foto: "imgs/artesanías/artesanía_03.jpg",
    tipoDePublicación: 'Exhibición',
    precio: "Producto no disponible para la venta"
  }
]