
var talleres = [{
    id: 1,
    nombre: "Taller de Origami",
    descripcion: "Taller con 12 años de antigüedad. Tenemos actividades para grandes y chicos, en todos los niveles.",
    foto: "imgs/origami.jpg",
    telefono: "4123-3214",
    horario: "De 08:00 a 12:00 hrs",
    actividades: "Origami para Principiantes, Origami Profesional",
    categoria: "Papel",
    coodenada_x: "-34.517853",
    coodenada_y: "-58.701276",
    direccion: "Pablo Rojas Paz 2100, Malvinas Argentinas",
  },
  {
    id: 2,
    nombre: "Taller de Pintura",
    descripcion: "Taller de pintura y dibujo para principiantes y avanzados. Con profesores profesionales.", 
    foto: "imgs/pint.jpg",
    telefono: "4451-8054",
    horario: "De 10:00 a 20:00 hrs",
    actividades: "Pintura, Dibujo",
    categoria: "Dibujo, Pintura",
    coodenada_x: "-34.518834",
    coodenada_y: "-58.704162",
    direccion: "Amenabar 1098, Malvinas Argentinas",
  },
  {
    id: 3,
    nombre: "Taller Ecoart",
    descripcion: "Taller pensado desde la ecología. Todo se recicla.",
    foto: "imgs/eco.jfif",
    telefono: "4664-7845",
    horario: "De 10:00 a 18:00 hrs",
    actividades: "Arte con elementos reciclados, Papel reciclado",
    categoria: "Papel, Plástico, Cartón, Otros",
    coodenada_x: "-34.520867",
    coodenada_y: "-58.705546",
    direccion: "Romain Rolland 2298, Malvinas Argentinas",

    
  },
  {
    id: 4,
    nombre: "Taller de Arte en Madera",
    descripcion: "Taller en madera, madera, madera y madera.",
    foto: "imgs/madera.jpg",
    telefono: "4664-9966",
    horario: "De 08:00 a 18:00 hrs",
    actividades: "Tallado en Madera, Más Madera",
    categoria: "Madera",
    coodenada_x: "-34.523475",
    coodenada_y: "-58.699924",
    direccion: "Verdi 1798, Malvinas Argentinas",

  },

  {
    id: 5,
    nombre: "Taller Artito",
    descripcion: "Taller muy divertido. El dueño se llama Tito Artito.",
    foto: "imgs/grab.jpg",
    telefono: "4664-9966",
    horario: "De 08:00 a 16:00 hrs",
    actividades: "Dibujo, Pintura, Grabado",
    categoria: "Dibujo, Pintura, Grabado",
    coodenada_x: "-34.522043",
    coodenada_y: "-58.705910",
    direccion: "Mozart 1098, Malvinas Argentinas",

  },

  {
    id: 6,
    nombre: "Taller de Arte Libre",
    descripcion: "Arte libre, para todos los gustos.",
    foto: "imgs/alfareria.jpg",
    telefono: "4664-9966",
    horario: "De 08:00 a 20:00 hrs",
    actividades: "Arcilla, Alfarería, Yasí",
    categoria: "Arcilla, Otros",
    coodenada_x: "-34.521194",
    coodenada_y: "-58.698980",
    direccion: "Jose Leon Suarez 1800, Malvinas Argentinas",

  },

  {
    id: 7,
    nombre: "Taller OtroTaller",
    descripcion: "Actividades de teñido de ropa (la ropa la trae el alumno..).",
    foto: "imgs/ropa.jpg",
    telefono: "4664-9966",
    horario: "De 08:00 a 12:00 hrs",
    actividades: "Teñido de ropa",
    categoria: "Ropa",
    coodenada_x: "-34.518825",
    coodenada_y: "-58.706358",
    direccion: "Verdi 1098, Malvinas Argentinas",

  } 

]